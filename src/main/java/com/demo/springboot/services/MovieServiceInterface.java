package com.demo.springboot.services;


import com.demo.springboot.dto.MovieListDto;

public interface MovieServiceInterface {
    void MovieApiController();
    MovieListDto getMovies();
    void addMovies(String title, int year, String image);
    void updateMovies(int id, String title, int year, String image);
    void deleteMovies(int id);
}
