package com.demo.springboot.rest;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.services.MovieService;
import org.apache.coyote.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

   MovieService movies = new MovieService();

    @PostMapping("/movies")
    public ResponseEntity<Void> createMovies(@RequestBody CreateMovieDto createMovieDto) throws URISyntaxException {
        LOG.info("--- id: {}", createMovieDto.getMovieId());
        LOG.info("--- title: {}", createMovieDto.getTitle());
        LOG.info("--- year: {}", createMovieDto.getYear());
        LOG.info("--- image: {}", createMovieDto.getImage());
        movies.addMovies(createMovieDto.getTitle(),createMovieDto.getYear(),createMovieDto.getImage());

        return ResponseEntity.created(new URI("/movies/" + createMovieDto.getMovieId())).build();
    }

    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        LOG.info("--- get all movies: {}", movies.getMovies());
        return ResponseEntity.ok().body(movies.getMovies());
    }

    @PutMapping("/movies/{id}")
    public ResponseEntity<Void> updateMovie(@RequestBody CreateMovieDto createMovieDto){
        LOG.info("---id:{}", createMovieDto.getMovieId());
        LOG.info("--- title: {}", createMovieDto.getTitle());
        LOG.info("--- year: {}", createMovieDto.getYear());
        LOG.info("--- image: {}", createMovieDto.getImage());

        movies.updateMovies(createMovieDto.getMovieId(), createMovieDto.getTitle(), createMovieDto.getYear(), createMovieDto.getImage());

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/movies/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable("id") Integer id){
        LOG.info("---id:{}",id);
        movies.deleteMovies(id);
        return ResponseEntity.ok().build();
    }
}
